﻿using System;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace eThorCodeTest
{
    public partial class eThorTestProject : Form
    {

        //Used to track whether the pollingThread has been started
        private Boolean _started;
        private Thread _pollingThread;

        //URL for the Test Project JSON service
        const string TestProjectServiceUrl = "http://test.ethorstat.com/test.ashx";

        public eThorTestProject()
        {
            InitializeComponent();
        }

        //The Start/Stop button has been clicked, toggle the state
        private void StartButtonClick(object sender, EventArgs e)
        {
            ToggleState();
        }


        //Seperated out ToggleState from the buttonClick event for the sake of clarity
        private void ToggleState()
        {
            //Flip the boolean
            _started = !_started;

            if (_started)
            {
                //Start the polling thread
                startButton.Text = "Stop";
                _pollingThread = new Thread(PollTestServer);
                _pollingThread.Start();
            }
            else
            {
                //Abort the polling thread
                startButton.Text = "Start";
                try
                {
                    _pollingThread.Abort();
                }
                catch (ThreadAbortException)
                {
                    //Do nothing, we intended to abort the thread so just suppress the exception
                }
                finally
                {
                    //Mark it null so it can be garbage collected if appropriate
                    _pollingThread = null;
                }

            }
        }

        //Thread delegate to poll the test server for two parameters and an operator
        private void PollTestServer()
        {
            //While running: poll, update the logBox and then sleep for 1 second
            while (_started)
            {
                var tpRes = _download_serialized_json_data<TestProjectResponse>(TestProjectServiceUrl);

                //Format the string that's being appended
                String formattedLogEntry = tpRes.parm1 + " " + tpRes.op + " " + tpRes.parm2;
                String formattedProduct;
                try
                {
                    formattedProduct =
                        parseOperatorAndReturnProduct(tpRes.parm1, tpRes.parm2, tpRes.op).ToString("0.###");
                }
                catch (DivideByZeroException)
                {
                    formattedProduct = "NaN";
                }
                formattedLogEntry += "\t= " + formattedProduct;

                //Append and sleep
                AppendToLogBox(formattedLogEntry);
                Thread.Sleep(1000);
            }
        }

        //Utility method to return the parsed result, ought to throw an exception for unhandled operators
        private Decimal parseOperatorAndReturnProduct(int p1, int p2, string op)
        {
            switch (op)
            {
                case "+":
                    return p1 + p2;
                case "-":
                    return p1 - p2;
                case "*":
                    return p1 * p2;
                case "/":
                    return Decimal.Divide(p1, p2);
                default:
                    return 0;
            }
        }

        //JSON deserialize method lifted directly from the Newtonsoft documentation
        private T _download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var jsonData = string.Empty;
                try
                {
                    jsonData = w.DownloadString(url);
                }
                catch (Exception e)
                { }

                //If it's a deserializable response then deserialize and return the object
                return !string.IsNullOrEmpty(jsonData) ? JsonConvert.DeserializeObject<T>(jsonData) : new T();
            }
        }

        //Thread-safe method to append a string to the logBox
        private void AppendToLogBox(string text)
        {
            try
            {
                if (logBox.InvokeRequired)
                {
                    Invoke(new Action<string>(AppendToLogBox), new object[] { text });
                }
                else
                {
                    //Append the text to the actual control
                    logBox.AppendText(text + "\r\n");
                }

            }
            catch (ObjectDisposedException ode)
            {
                /* The polling thread is trying to update the logBox as the window is closing,
                 * abort the thread and suppress the exception. This oughn't happen as the Abort()
                 * is also called in the overridden Dispose() method for the frame.*/
                _pollingThread.Abort();
            }
        }
    }

    //Used to deserialize JSON responses and stuff the values in an object
    public class TestProjectResponse
    {
        public int parm1 { get; set; }
        public int parm2 { get; set; }
        public string op { get; set; }
    }
}
